# README #

This package contains some simple solutions to common pentest issues that were repported in actual pentests.

### PACKAGE ###

The package adds the following items to your solution:

* A configuration file to set some parameters
* A module that checks requests
* Some transformations for the web.config

### ISSUES ADDRESSED ###

* Strict-Transport-Security - forcing IE to process subsequent requests on SSL for a specific duration
* cross-frame-scripting protection - Disallows iFramimg of the website except for paths specified in the configuration file
* Sets several timeout settings to 20 minutes (umbraco, session and form)
* Forces use of SSL and HTTP-only cookies
* Turns on custom errors
* Disables trace
* Removes unneeded HTTP headers
* Turns off debug and batch
* Disallows the OPTIONS verb on requests
* Sets no-sniff on HTTP headers
* Forces switch to SSL