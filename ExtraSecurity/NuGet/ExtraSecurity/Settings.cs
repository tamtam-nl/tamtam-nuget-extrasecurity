﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml.Linq;

namespace TamTam.NuGet.ExtraSecurity
{
    /// <summary>
    /// LogCleaner settings from configuration file
    /// </summary>
    public static class Settings
    {
        public static List<string> AllowedIFrameRegexes;

        static Settings()
        {
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.ExtraSecurity.config");
            var settings = XDocument.Load(file).Element("settings");
            var allowedIFramePaths = settings.Element("allowedIframePaths").Elements("add");
            AllowedIFrameRegexes = new List<string>();
            foreach (var add in allowedIFramePaths)
            {
                var path = add.GetValue("regex");
                AllowedIFrameRegexes.Add(path);
            }
        }

        /// <summary>
        /// Extension methof that returns the value of an attribute in an element if the attribute exists and does not have an empty or null string
        /// </summary>
        /// <param name="e">the extended object</param>
        /// <param name="name">the name of the attribute</param>
        /// <returns></returns>
        private static string GetValue(this XElement e, string name)
        {
            var attribute = e.Attribute(name);
            if (attribute != null)
            {
                var value = attribute.Value;
                if (!string.IsNullOrEmpty(value))
                {
                    return value;
                }
            }
            return null;
        }

    }

}
