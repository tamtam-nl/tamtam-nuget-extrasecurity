﻿using System;
using System.Web;
using System.Linq;
using System.Text.RegularExpressions;

namespace TamTam.NuGet.ExtraSecurity
{
    public class Module : IHttpModule
    {
        public void Init(HttpApplication application)
        {
            application.PreSendRequestHeaders += application_PreSendRequestHeaders;
        }

        void application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            if (context != null)
            {
                var request = context.Request;
                var response = context.Response;
                if (request.Url.Scheme.Equals("https"))
                {
                    response.AddHeader("Strict-Transport-Security", "max-age=31536000");
                }

                //Adding cross-frame-scripting protection
                if (!Settings.AllowedIFrameRegexes.Any(r => Regex.IsMatch(request.Url.AbsolutePath, r, RegexOptions.IgnoreCase)))
                {
                    response.Headers.Add("X-Frame-Options", "DENY");
                }
            }
        }

        public void Dispose()
        {
        }
    }
}