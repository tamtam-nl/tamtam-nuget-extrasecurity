﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ExtraSecurity")]
[assembly: AssemblyDescription("Adds extra security measures for PEN tests:" +
      "- Set all timeouts to 20 minutes" +
      "- Forces SSL on all requests" +
      "- Enforces HTTPOnly and SSL cookies" +
      "- Clears unneeded HTTP header information" +
      "- Disables iFraming and adds Strict-Transport-Security to HTTP header" +
      "- Contains samples for Umbraco")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Tam Tam")]
[assembly: AssemblyProduct("ExtraSecurity")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("67148e23-1adf-46c2-bb59-c4dbf8fb701a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
